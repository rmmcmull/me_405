## @file LAB_0X01.py
#  @brief This code embodies the function of a basic vending machine which was designed in HW0x00.
#  @details Vendotron, a python vending machine, can accept payment ranging from pennies to 
#           twenty dollar bills. The user can then select from 4 beverages or eject their balance. 
#           The user can use the keyboard to interface with Vendotron.
#  @author Ryan McMullen
#  @copyright simpout controls
#  @date Jan. 17, 2020
#  Latest Update:
#  @date Jan. 20, 2020

import keyboard

## marker used to transition to various states
state = 0

# parameteric values
## price of 1 cuke
cuke_price = 100
## price of 1 popsi
popsi_price = 120 
## price of 1 spryte
spryte_price = 85 
## price of 1 pupper
pupper_price = 110 


## Define Functions 

def printWelcome():
    '''
    @brief This function prints a Vendotron welcome message.
    @details 	the message is as follows
	            Welcome to Vendotron!
	            Avaliable Beverages:
	            Cuke        $1.00
	            Popsi       $1.20
	            Spryte      $0.85
	            Dr. Pupper  $1.10
	            
	            *Use Keypad to interface
	            with vending machine.
	            
	            To Insert Money:
	                0 = Penny
	                1 = Nickle
	                2 = Dime
	                3 = Quarter
	                4 = 1 Dollar
	                5 = 5 Dollars
	                6 = 10 Dollars
	                7 = 20 Dollars
	                
	            To Select Item:
	                shift + e = Eject
	                shift + c = Cuke
	                shift + p = Popsi
	                shift + s = Spryte
	                shift + d = Dr. Pupper
    '''
    
    print("""
    Welcome to Vendotron!
    Avaliable Beverages:
    Cuke        $1.00
    Popsi       $1.20
    Spryte      $0.85
    Dr. Pupper  $1.10
    
    *Use Keypad to interface
    with vending machine.
    
    To Insert Money:
        0 = Penny
        1 = Nickle
        2 = Dime
        3 = Quarter
        4 = 1 Dollar
        5 = 5 Dollars
        6 = 10 Dollars
        7 = 20 Dollars
        
    To Select Item:
        shift + e = Eject
        shift + c = Cuke
        shift + p = Popsi
        shift + s = Spryte
        shift + d = Dr. Pupper
        """)
    
    pass

def getChange(price, payment):    
    '''
    @brief computes change for monetary transaction
    @details computes change given set of bills and coins ranging from pennies
             to twenty dollar bills
    @param price the price of the item as an integer number of cents
    @param payment tuple which represents the type of currency entered
    @return if funds are sufficient, function returns a tuple of bills/coins;
            if insufficient, returns none.
    '''
	## tuple which represents payment, in cents, of each respective currency input
    amount = (1, 5, 10, 25, 100, 500, 1000, 2000) 
	
	## balance in vending machine before any money has been put in
    balance = 0
    for pay,num in zip(payment, amount): # Index through payment and amount tuples simultaniously
        balance += pay*num # calculates total balance, in cents
    balance -= price # calculates net balance, in cents
	# twenty dollar bill (initial)
    twenty = 0
	# ten dollar bill (initial)
    ten = 0
	# five dollar bill (initial)
    five = 0 
	# one dollar bill (initial)
    one = 0
	# quarter
    quarter = 0
	# nickel (initial)
    nickel = 0
	# dime (initial)
    dime = 0
	# penny (initial)
    penny = 0

    if balance < 0:
        
        return 'None'
    
    else:
        while balance > 0:
            
            if balance >= 2000:
                balance -= 2000 # if balance is less than a twenty value
                twenty += 1 # tabulate number of twenty
                
            elif balance >= 1000: # if balance is less than a ten value
                balance -= 1000 # dispense a ten
                ten += 1 # tabulate number of quarters
                
            elif balance >= 500: # if balance is less than a five value
                balance -= 500 # dispense a five
                five += 1 # tabulate number of quarters
                
            elif balance >= 100:# if balance is less than a one value
                balance -= 100 # dispense a one
                one += 1 # tabulate number of quarters22P
                
            elif balance >= 25: # if balance is less than a quarter value
                balance -= 25 # dispense a quarter
                quarter += 1 # tabulate number of quarters
                
            elif balance >= 10: # if balance is less than a dime
                balance -= 10 # dispense a dime
                dime += 1 # tabulate number of dimes in output
                
            elif balance >= 5: # if balance is less than a nickel
                balance -= 5 # dispense a nickel
                nickel += 1 # tabulate number of nickels in output
                
            else: 
                balance -= 1 # dispense a penny
                penny += 1 # tabulate number of pennies
    ## change provided (as a tuple)
    change = (penny, nickel, dime, quarter, one, five, ten, twenty)

    print(
          'Change provided: ' + str(change[7]) + ' twenty dollar bills, '+ 
          str(change[6]) + ' ten dollar bills, '
          + str(change[5]) + ' five dollar bills, '
          + str(change[4]) + ' one dollar bills, '
          + str(change[3]) + ' quarters, ' + str(change[2]) + 
          ' dimes, ' + str(change[1]) + ' nickels, ' + 'and ' + str(change[0]) +
          ' pennies!')
    
    return change

## FSM below. while True executes a loop which runs forever
while True:
    try: # try block allows code to be tested and works with except block to allow
         # user to handle certain errors
         
        # FSM always goes into state 0 (init state) first
        if  state == 0: # Initilization State
			# print startup message (in the REPL)
            printWelcome() 
			## describes what type of payment has been issued
            payment = () 
			# balance of user
            balance = 0 
			## twenty dollar bill
            twenty = 0 
			## ten dollar bill
            ten = 0 
			## five dollar bill
            five = 0 
			## one dollar bill
            one = 0 
			## quarter
            quarter = 0 
			## nickel
            nickel = 0 
			## dime
            dime = 0 
			## penny
            penny = 0
			## indicates if a cuke has been selected
            cuke_selected = 0 
			## indicates if a popsi has been selected
            popsi_selected = 0
			## indicates if a spryte has been selected
            spryte_selected = 0
			## indicates if a pupper has been selected
            pupper_selected = 0
			## boolean that indicates if the user has opted to eject
            eject = 0 
            state = 1 # once initialized, always transition to state 1
        
            
        elif state == 1: # ready for Action State
                         # this state interperets inputs such as inserted change
                         # and selection/ejection
    
            if keyboard.is_pressed ('0'):
                penny += 1 # add 1 to penny integer every time 0 is pressed on keyboard
                state = 2 # transition to state 2 which calculates balance
            elif keyboard.is_pressed ('2'):
                dime += 1 # add 1 to dime integer every time 0 is pressed on keyboard
                state = 2 # transition to state 2 which calculates balance
            elif keyboard.is_pressed ('1'):
                nickel += 1 # add 1 to nickel integer every time 0 is pressed on keyboard
                state = 2 # transition to state 2 which calculates balance
            elif keyboard.is_pressed ('3'):
                quarter += 1 # add 1 to quarter integer every time 0 is pressed on keyboard
                state = 2 # transition to state 2 which calculates balance
            elif keyboard.is_pressed ('4'):
                one += 1 # add 1 to one integer every time 0 is pressed on keyboard
                state = 2 # transition to state 2 which calculates balance
            elif keyboard.is_pressed ('5'):
                five += 1 # add 1 to five integer every time 0 is pressed on keyboard
                state = 2 # transition to state 2 which calculates balance
            elif keyboard.is_pressed ('6'):
                ten += 1 # add 1 to ten integer every time 0 is pressed on keyboard
                state = 2 # transition to state 2 which calculates balance
            elif keyboard.is_pressed ('7'):
                twenty += 1 # add 1 to twenty integer every time 0 is pressed on keyboard
                state = 2 # transition to state 2 which calculates balance
            elif keyboard.is_pressed ('shift + c'):
                cuke_selected = 1 # set variable if shift-c has been pressed on keyboard
                state = 3 # transition to state 3 which interprets input
            elif keyboard.is_pressed ('shift + p'):
                popsi_selected = 1 # set variable if shift-p has been pressed on keyboard
                state = 3 # transition to state 3 which interprets input
            elif keyboard.is_pressed ('shift + s'):
                spryte_selected = 1 # set variable if shift-s has been pressed on keyboard
                state = 3 # transition to state 3 which interprets input
            elif keyboard.is_pressed ('shift + d'):
                pupper_selected = 1 # set variable if shift-d has been pressed on keyboard
                state = 3 # transition to state 3 which interprets input
            elif keyboard.is_pressed ('shift + e'):
                eject = 1 # set variable if shift-e has been pressed on keyboard
                state = 3 # transition to state 3 which interprets input
                
        elif state == 2:   # calculates payment that has been entered
            if not keyboard.is_pressed ('0') \
                and not keyboard.is_pressed ('1') \
                    and not keyboard.is_pressed ('2') \
                        and not keyboard.is_pressed ('3') \
                            and not keyboard.is_pressed ('4') \
                                and not keyboard.is_pressed ('5') \
                                    and not keyboard.is_pressed ('6') \
                                        and not keyboard.is_pressed ('7'):
                                            payment = (penny, nickel, dime, quarter, one, five, ten, twenty)
                                            ## tuple which shows the associated value of each payment type in cents
                                            amount = (1, 5, 10, 25, 100, 500, 1000, 2000)
                                            balance = 0
                                            for pay,num in zip(payment, amount): 
                                                balance += pay*num
                                            print('balance = ' + str(balance) + ' [cents]')
                                            state = 1
                                            
                                        
        elif state == 3: # intermediate state to prvent user from holding down key
            if not keyboard.is_pressed ('shift + c') \
                and not keyboard.is_pressed ('shift + p') \
                    and not keyboard.is_pressed ('shift + s') \
                        and not keyboard.is_pressed ('shift + d') \
                            and not keyboard.is_pressed ('shift + e'):
                            state = 4
                            
        elif state == 4: # beverage was selected for purchase. If balance is >=
                         # cost, call getChange and clear the selected beverage
            if cuke_selected == 1 and balance >= cuke_price: 
                getChange(cuke_price,payment)
                cuke_selected = 0
                state = 5
            elif popsi_selected == 1 and balance >= popsi_price:
                getChange(popsi_price,payment)
                popsi_selected = 0
                state = 5
            elif spryte_selected == 1 and balance >= spryte_price:
                getChange(spryte_price,payment)
                spryte_selected = 0
                state = 5
            elif pupper_selected == 1:
                getChange(pupper_price,payment)
                pupper_selected = 0
                state = 5
            elif eject == 1:
                getChange(0,payment)
                state = 5
            else: # if user selects beverage for purchase and does not have 
                  # sufficient balance, print warning and return to state 1
                print('''
    balance not sufficient
    please enter more money
                      ''')
                state = 1
        
        elif state == 5:
            state = 0
        # when control-c is pressed, break and print message
    except KeyboardInterrupt:
        break
    
print ('Control-C has been pressed, so it is time to exit.')