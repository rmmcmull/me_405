var searchData=
[
  ['adc_1',['adc',['../namespacemain__backend.html#a4dcbd9670b2d9535a1a6861ba1bb9f4c',1,'main_backend']]],
  ['adcall_2',['adcall',['../namespacemain.html#aa10ef44f24a5e954a6b405bbbb743b4a',1,'main']]],
  ['address_3',['address',['../classmcp9808_1_1mcp9808.html#a2f44cead8ac0fa0587e52a7e71bb5019',1,'mcp9808::mcp9808']]],
  ['amb_5ftemp_4',['amb_temp',['../namespacemain.html#a9eacb8cfcf06cc57e9f97cd6a279676a',1,'main']]],
  ['amb_5ftemps_5',['amb_temps',['../namespacetemperature__plotter.html#a3ebfd25c7a7303d692f2492a3fc6d6ac',1,'temperature_plotter']]],
  ['amount_6',['amount',['../namespaceLAB__0X01.html#ac0a87c361097e26451b520ae6ec8fa0f',1,'LAB_0X01']]],
  ['avg_5ftime_7',['avg_time',['../namespaceLAB__0X02.html#a364931fb795f1009c45ed49fc070fbe2',1,'LAB_0X02']]],
  ['avgtimefct_8',['avgTimeFCT',['../namespaceLAB__0X02.html#a71d8e9f6e2de5a9a094cc90fa3f6f8b0',1,'LAB_0X02']]]
];
