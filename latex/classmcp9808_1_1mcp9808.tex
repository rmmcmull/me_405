\hypertarget{classmcp9808_1_1mcp9808}{}\doxysection{mcp9808.\+mcp9808 Class Reference}
\label{classmcp9808_1_1mcp9808}\index{mcp9808.mcp9808@{mcp9808.mcp9808}}
\doxysubsection*{Public Member Functions}
\begin{DoxyCompactItemize}
\item 
def \mbox{\hyperlink{classmcp9808_1_1mcp9808_afff35ec721ca92214abe68c409c75947}{\+\_\+\+\_\+init\+\_\+\+\_\+}} (self, I2C, Address)
\item 
def \mbox{\hyperlink{classmcp9808_1_1mcp9808_a88d1f2c040cc310d72a26e0cda0c99bc}{check}} (self)
\item 
def \mbox{\hyperlink{classmcp9808_1_1mcp9808_a2aa3aa0a45168d84968c7a36d7cdd542}{celsius}} (self)
\item 
def \mbox{\hyperlink{classmcp9808_1_1mcp9808_a8839f3103b0fc00f4e1ffd47c598d75c}{fahrenheit}} (self)
\end{DoxyCompactItemize}
\doxysubsection*{Public Attributes}
\begin{DoxyCompactItemize}
\item 
\mbox{\hyperlink{classmcp9808_1_1mcp9808_a2f44cead8ac0fa0587e52a7e71bb5019}{address}}
\begin{DoxyCompactList}\small\item\em address of where the i2c object is located on the bus \end{DoxyCompactList}\item 
\mbox{\hyperlink{classmcp9808_1_1mcp9808_a4297bd23f4d3de9980ca1444461bad13}{i2c}}
\begin{DoxyCompactList}\small\item\em pass in the i2c object so it doesn\textquotesingle{}t need to be passed in method \end{DoxyCompactList}\item 
\mbox{\hyperlink{classmcp9808_1_1mcp9808_a23bb5890e449b0fad485069eccbeae56}{temp}}
\begin{DoxyCompactList}\small\item\em with register pointer set to 5, will return temperature date. \end{DoxyCompactList}\end{DoxyCompactItemize}


\doxysubsection{Detailed Description}
\begin{DoxyVerb}@brief allows the user to communicate with an MCP9808 temperature sensor
       using its I2C interface. It contains 3 methods, which are shown below.
\end{DoxyVerb}
 

\doxysubsection{Constructor \& Destructor Documentation}
\mbox{\Hypertarget{classmcp9808_1_1mcp9808_afff35ec721ca92214abe68c409c75947}\label{classmcp9808_1_1mcp9808_afff35ec721ca92214abe68c409c75947}} 
\index{mcp9808.mcp9808@{mcp9808.mcp9808}!\_\_init\_\_@{\_\_init\_\_}}
\index{\_\_init\_\_@{\_\_init\_\_}!mcp9808.mcp9808@{mcp9808.mcp9808}}
\doxysubsubsection{\texorpdfstring{\_\_init\_\_()}{\_\_init\_\_()}}
{\footnotesize\ttfamily def mcp9808.\+mcp9808.\+\_\+\+\_\+init\+\_\+\+\_\+ (\begin{DoxyParamCaption}\item[{}]{self,  }\item[{}]{I2C,  }\item[{}]{Address }\end{DoxyParamCaption})}

\begin{DoxyVerb}@brief creates an I2C object 
\end{DoxyVerb}
 

\doxysubsection{Member Function Documentation}
\mbox{\Hypertarget{classmcp9808_1_1mcp9808_a2aa3aa0a45168d84968c7a36d7cdd542}\label{classmcp9808_1_1mcp9808_a2aa3aa0a45168d84968c7a36d7cdd542}} 
\index{mcp9808.mcp9808@{mcp9808.mcp9808}!celsius@{celsius}}
\index{celsius@{celsius}!mcp9808.mcp9808@{mcp9808.mcp9808}}
\doxysubsubsection{\texorpdfstring{celsius()}{celsius()}}
{\footnotesize\ttfamily def mcp9808.\+mcp9808.\+celsius (\begin{DoxyParamCaption}\item[{}]{self }\end{DoxyParamCaption})}

\begin{DoxyVerb}@brief method returns the measured temperature in degrees celsius.
       The following code was found at:
       https://learn.adafruit.com/micropython-hardware-i2c-devices/i2c-main
       and is used to pull out a signed 12-bit value in degrees celsius
       with a simple fuction from the mcp9808.
\end{DoxyVerb}
 \mbox{\Hypertarget{classmcp9808_1_1mcp9808_a88d1f2c040cc310d72a26e0cda0c99bc}\label{classmcp9808_1_1mcp9808_a88d1f2c040cc310d72a26e0cda0c99bc}} 
\index{mcp9808.mcp9808@{mcp9808.mcp9808}!check@{check}}
\index{check@{check}!mcp9808.mcp9808@{mcp9808.mcp9808}}
\doxysubsubsection{\texorpdfstring{check()}{check()}}
{\footnotesize\ttfamily def mcp9808.\+mcp9808.\+check (\begin{DoxyParamCaption}\item[{}]{self }\end{DoxyParamCaption})}

\begin{DoxyVerb}        @brief a check to verify that the sensor is attached at the given bus 
               address by checking that the value in the manufacturer ID 
               register is correct. See data sheet pages (16-17) to double 
               check this information. If the correct bus address is used, the
               module will 1. If the incorrect bus address is used, will return
               a 0.\end{DoxyVerb}
 \mbox{\Hypertarget{classmcp9808_1_1mcp9808_a8839f3103b0fc00f4e1ffd47c598d75c}\label{classmcp9808_1_1mcp9808_a8839f3103b0fc00f4e1ffd47c598d75c}} 
\index{mcp9808.mcp9808@{mcp9808.mcp9808}!fahrenheit@{fahrenheit}}
\index{fahrenheit@{fahrenheit}!mcp9808.mcp9808@{mcp9808.mcp9808}}
\doxysubsubsection{\texorpdfstring{fahrenheit()}{fahrenheit()}}
{\footnotesize\ttfamily def mcp9808.\+mcp9808.\+fahrenheit (\begin{DoxyParamCaption}\item[{}]{self }\end{DoxyParamCaption})}

\begin{DoxyVerb}@brief method returns the measured temperature in degrees fahrenheit.
       The following code was found at:
       https://learn.adafruit.com/micropython-hardware-i2c-devices/i2c-main
       and is used to pull out a signed 12-bit value in degrees celsius
       with a simple fuction from the mcp9808.
\end{DoxyVerb}
 

\doxysubsection{Member Data Documentation}
\mbox{\Hypertarget{classmcp9808_1_1mcp9808_a2f44cead8ac0fa0587e52a7e71bb5019}\label{classmcp9808_1_1mcp9808_a2f44cead8ac0fa0587e52a7e71bb5019}} 
\index{mcp9808.mcp9808@{mcp9808.mcp9808}!address@{address}}
\index{address@{address}!mcp9808.mcp9808@{mcp9808.mcp9808}}
\doxysubsubsection{\texorpdfstring{address}{address}}
{\footnotesize\ttfamily mcp9808.\+mcp9808.\+address}



address of where the i2c object is located on the bus 

\mbox{\Hypertarget{classmcp9808_1_1mcp9808_a4297bd23f4d3de9980ca1444461bad13}\label{classmcp9808_1_1mcp9808_a4297bd23f4d3de9980ca1444461bad13}} 
\index{mcp9808.mcp9808@{mcp9808.mcp9808}!i2c@{i2c}}
\index{i2c@{i2c}!mcp9808.mcp9808@{mcp9808.mcp9808}}
\doxysubsubsection{\texorpdfstring{i2c}{i2c}}
{\footnotesize\ttfamily mcp9808.\+mcp9808.\+i2c}



pass in the i2c object so it doesn\textquotesingle{}t need to be passed in method 

\mbox{\Hypertarget{classmcp9808_1_1mcp9808_a23bb5890e449b0fad485069eccbeae56}\label{classmcp9808_1_1mcp9808_a23bb5890e449b0fad485069eccbeae56}} 
\index{mcp9808.mcp9808@{mcp9808.mcp9808}!temp@{temp}}
\index{temp@{temp}!mcp9808.mcp9808@{mcp9808.mcp9808}}
\doxysubsubsection{\texorpdfstring{temp}{temp}}
{\footnotesize\ttfamily mcp9808.\+mcp9808.\+temp}



with register pointer set to 5, will return temperature date. 

convert temperature to farenheit from celsius

See page 16 of data sheet for more information.

bitwise left shift to the left and bitwise or operator

bitwise and divided by 16 