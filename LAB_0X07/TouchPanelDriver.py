'''
@file TouchPanelDriver.py

@brief This code creates a python class which can read the position on 
            a resistive touch pad. It contains a constructor which allows the 
            user to select four abritrary pins representing xp, xm, yp,ym as 
            well as the width and length of the resistive touch panel and the 
            coordinate representing the center of the resisitve touch panel. In
            addition, it contains 3 methods which scan and return the postion
            of the X,Y,and Z components, respectively. Finally, this class 
            contains a method which reads all 3 components and returns their 
            values as a tuple. Every channel read takes less than 500us. 

@author Ryan McMullen and David Meyenberg

@copyright simpout-controls

@date March 17, 2021
'''

import pyb
import utime

# define timer 2 for general purpose counting at a frequency of 10Hz
## Timer used to test scanning speeds 
tim = pyb.Timer(2, prescaler = 79, period = 0x7FFFFFFF)

class touchy:
    '''
    Allows the user to read x, y, and z position on a touch panel.
    '''

    def __init__(self,Xp,Xm,Yp,Ym,x_overall,y_overall,x_center,y_center):
        '''
        @brief Creates a touch panel object 
		@param Xp positive X pin
		@param Xm negative X pin 
		@param Yp positive Y pin
		@param Ym negative Y pin
		@param x_overall width of panel
		@param y_overall height of panel
		@param x_center center of panel in x direction
		@param y_center center of panel in y direction
        '''
        
        # define xp, which in our case will be configured as a push-pull output
        # and asserted to be high
		## X+ pin
        self.x_p = Xp
        
        # define xm, which in our case will be configured as a push-pull output
        # and asserted to be low
		## X- pin
        self.x_m = Xm
        
        # define yp, which in our case will be configured as a floating pin
        ## Y+ pin
        self.y_p = Yp
        
        # define ym, which in our case will be configured as an ADC pin, used
        # to measure the voltage from the touch pad.
        ## Y- pin
        self.y_m =Ym
        
        # overall width in the x-direction (x is parallel to edge which is longer)
        ## Width of panel
        self.x_tot = x_overall
        
        # overall width in the y-direction (y is parallel to edge which is shorter)
        ## Height of panel
        self.y_tot = y_overall
        
        # x center of resistive touch panel
        ## x center of panel
        self.x_cent = x_center
        
        # y center of resistive touch panel
		## Center of panel
        self.y_cent = y_center
        
        
    def x_read(self):
        '''
        @brief Scans the x component of the resistive touch pad
        '''
        # set counter value to zero
        tim.counter(0) 
        
        ## Time at start
        start_time = tim.counter() 
        
        # push-pull output and asserted to high
		## YP pin object
        yp = pyb.Pin(self.y_p,mode=pyb.Pin.OUT_PP)
        yp.high()
        
        # push-pull output and asserted to high
        ## YM pin object
        ym = pyb.Pin(self.y_m,mode=pyb.Pin.OUT_PP)
        ym.low()
        
        # ADC, which can be used to read the voltage and get corresponding 
        # x position
        ## XM pin object
        xm = pyb.ADC(self.x_m)
        
        # set as float
		## XP pin object
        xp = pyb.Pin(self.x_p,mode = pyb.Pin.IN)
    
        # as explained in the lab manual, the resistive touch panel is modeled
        # as a purely resistive element, but in reality has some inductance and
        # capacitance which require a settling time before data collection. 
        # The lab instructor measured the signal to take approximately 3.6 
        # micro seconds to settle.
        utime.sleep_us(4) 
        
        # stop counter
		## Time at end
        stop_time = tim.counter() 
        
        #calculate total time
		## Total time
        tot_time = stop_time-start_time
        
        # return x coordinate [inches] calibrated these values by hand. 
        # width = 6.059", max-min counts = 4000, x-center = 3.4795"
        return 25.47*(xm.read()*6.959/(4000)-3.4795)
    
        
        
    def y_read(self):
        '''
        @brief Scans the y component of the resistive touch pad
        '''
         # set counter value to zero
        tim.counter(0) 
        
        # start the timer
        start_time = tim.counter() 
        
        # push-pull output and asserted to high
        xp = pyb.Pin(self.x_p,mode=pyb.Pin.OUT_PP)
        xp.high()
        
        # push-pull output and asserted to high
        xm = pyb.Pin(self.x_m,mode=pyb.Pin.OUT_PP)
        xm.low()
        
        # ADC, which can be used to read the voltage and get corresponding 
        # x position
        ym = pyb.ADC(self.y_m)
        
        # set as float
        yp = pyb.Pin(self.y_p,mode = pyb.Pin.IN)
        
        # as explained in the lab manual, the resistive touch panel is modeled
        # as a purely resistive element, but in reality has some inductance and
        # capacitance which require a settling time before data collection. 
        # The lab instructor measured the signal to take approximately 3.6 
        # micro seconds to settle.
        utime.sleep_us(4) 
        
        # stop counter
        stop_time = tim.counter() 
        
        #calculate total time
        tot_time = stop_time-start_time
        
        # return y coordinate [inches] calibrated these values by hand. 
        # active area width = 3.93701", max/min counts = 3685/553, x-center = 1.968
        return 25.47*(ym.read()*3.93701/(4000)-1.96851)
        
      
    def z_read(self):
        '''
        @brief Scans the z component of the resistive touch pad
        '''
        # set counter value to zero
        tim.counter(0) 
        
        # start the timer
        start_time = tim.counter() 
        
        # push-pull output and asserted to high
        yp = pyb.Pin(self.y_p,mode=pyb.Pin.OUT_PP)
        yp.high()
        
        # push-pull output and asserted to high
        xm = pyb.Pin(self.x_m,mode=pyb.Pin.OUT_PP)
        xm.low()
        
        # ADC, which can be used to read the voltage and get corresponding 
        # x position
        ym = pyb.ADC(self.y_m)
        
        # set as float
        yp = pyb.Pin(self.y_p,mode = pyb.Pin.IN)
        
        # as explained in the lab manual, the resistive touch panel is modeled
        # as a purely resistive element, but in reality has some inductance and
        # capacitance which require a settling time before data collection. 
        # The lab instructor measured the signal to take approximately 3.6 
        # micro seconds to settle.
        utime.sleep_us(4) 
        
        # stop counter
        stop_time = tim.counter() 
        
        #calculate total time
        tot_time = stop_time-start_time
        
        # return z coordinate [true/false]
        return ym.read() < 4000
    
    def read_data(self):
       '''
       @brief Scans all 3 coordinates and returns their values as a tuple
       '''
       
       ## Tuple to store X, Y, and Z scan values
       coord = (self.x_read(),self.y_read(),self.z_read())
       
       # return tuple which contains all the good stuff
       return coord
