## @file mainpage.py
#  Brief doc for mainpage.py

#  Detailed doc for mainpage.py 
#
#  @mainpage
#
#  @section sec_port Portfolio Details
#  This is my ME 405 portfolio. See individual modules for details
#
#  Included Modules are:
#  * Lab0X00 (\ref LAB_0X01)
#  * Lab0X01 (\ref LAB_0X02)
#  * Lab0X02 (\ref LAB_0X03)
#
#  @section sec_intro Introduction
#
#  @section sec_lab1 Lab 0X01 Documentation
#  * Source: https://bitbucket.org/rmmcmull/me_405/src/master/LAB_0x01/LAB_0X01.py
#  * @section sec_intro Lab 0X01
#    Vendotron, a python vending machine, can accept payment ranging from pennies to 
#    twenty dollar bills. The user can select from 4 beverages or eject their balance. 
#    The user can use the keyboard to interface with Vendotron and buy drinks.
#
#  @section sec_lab2 Lab 0X02 Documentation
#  * Source: https://bitbucket.org/rmmcmull/me_405/src/master/LAB_0x02/LAB_0X02.py
#  * Documentation: \ref Lab2
#  * This program waits a random time between 2 and 3 seconds, then
#    turns on the little green LED connected to Pin PA5 on the Nucleo
#    L476RG Microcontroller. Once the LED is turned on, a timer starts
#    which counts in microseconds. The LED will only remain on for 1 
#    second. An external interrupt is set to trigger on the falling edge
#    on PC13, which occurs when the blue "user" button on the Nucleo is
#    pressed. The interupt callback function then reads the timer to 
#    see how many microseconds have elapsed since the LED was turned
#    on. This process is repeated until the user presses Ctrl+C to 
#    stop the program. When the program is stopped, the average reaction
#    time over the number of tries is calculated and displayed. 
#    If no reaction time was successfully measured (such as the button
#    wasn't pressed), the program wil return an error message. This program
#    can be used as a reaction time test. The timer has resolution 
#    up to 1 microsecond. Keep in mind.. the averege reaction time for humans
#    is 0.25 [s].
#
#  @section sec_lab3 Lab 0X03 Documentation
#  * Source: https://bitbucket.org/rmmcmull/me_405/src/master/LAB_0X03/
#  * Documentation: \ref Lab3
#  * This program utilizes serial communication between the nucleo microcontroller
#    and spyder. Nucleo and spyder work together to capture button bounce data.
#
#
#  @section sec_lab4 Lab 0X04 Documentation
#  * Source: https://bitbucket.org/rmmcmull/cpe-mech-team/src/master/
#  * Documentation: \ref Lab4
#  * This program uses a mcp9808 breakout board to read ambient temperature
#    data. The python script \ref mcp9808.py interfaces and grabs data from the 
#    temperature sensor. \ref main.py then writes this data, along with the 
#    microcontroller temperature and associated time stamp to a .csv file on
#    the microcontroller. \ref temperature_plotter.py then reads the .csv file
#    and plots the temperature. I left the sensor running on my desk for 1 hour
#    next to an open window. The plotted results (\ref 1hourTemperaturePlot.PNG
#    can be found at the following repository location: 
#    
#
#  @author Ryan McMullen
#
#  @copyright simpout-controls
#
#  @date January 26, 2021
#