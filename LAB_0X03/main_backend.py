'''
@file main_backend.py

@brief This file runs on the nucleo board as a backend that will communicate with the 
        front end code, running from spyder. 
        
@details This code waits until an ASCII "G" has been sent from spyder and then 
        waits for a button push. Once a button has been pushed, it captures 
        a batch of voltage data and an associated time stamp at a sampling rate 
        of 10,000 Hz. It then sends this information, serially, back to sypder.
        Note that there needs to be a wire connecting PC13 (blue button) to A0!
        B1 push button is connected to PC13 and PC13 is pin 27 on the array CN7.
        The on-board LED will turn on after a delay of 4 seconds. As soon as the 
        LED goes high, the user must push the button as fast as possible, as was 
        done in the reaction time lab 2. The delay after the LED goes high may need
        to be adjusted based on reaction time. It has been tuned for a reaction 
        time of .25s, which is average.

@author Ryan McMullen

@copyright simpout controls

@date February 5, 2021
'''

import pyb, array, utime
from pyb import UART 

## timer object used for reading analog values... 50,000 Hz (20 microsecond period)
tim = pyb.Timer(2,freq=50000) 
## pin C13 on Nucleo must be an input or there can be a conflict (blue button). NOTE: Button not pressed = 3.3V, Button pressed = 0V
pinC13 = pyb.Pin(pyb.Pin.board.PC13, pyb.Pin.IN) 
## create an analog object from pin AO
adc = pyb.ADC(pyb.Pin.board.PA0) 
## define pin A5 on the nucleo (Green LED) 
pinA5 = pyb.Pin (pyb.Pin.cpu.A5, pyb.Pin.OUT_PP) 
## Buffer where ADC values are stored with associated time stamp from tim.
buf = array.array('H',(0 for n in range (20000))) 
# number of data points is 25,000 which at a sample rate of 50,000 Hz should give a 0.5 second recording.

## used to transfer data to spyder
myuart = UART(2)
                    
# Pin A0 is a 12 bit A/D giving a 'count' range of  0 to 2^12 (0 to 4095) or 4096 counter values. Therefor 0V corresponds to a count 
# of 0 and 3.3V (approx. depending on actual power supply voltage) corresponds to a count of 4095. So the approx step between each count is:
## conversion from count from ADC to volts
step = 3.3/4096 # [volts/count]
pinA5.low()   # Make sure LED is OFF 

while True:
    #if myuart.any() != 0: # anything not equal to 0
		## voltage data which is to be transferred to back end
        val = myuart.readchar() 
        # if value is G (ASCII 71), actively look for a button push
        if val == 71: # if "G" has been sent from spyder
            utime.sleep_ms(4000) # delay for 4 seconds                          
            pinA5.high() # turn on LED
            utime.sleep_ms(150) # delay for 4 seconds  
            adc.read_timed(buf,tim) # grab 1 second of data at 100,000 Hz sample rate                    
            for n in range (len(buf)): # loop n = len(buf) times
                myuart.write('{:}\r\n'.format(buf[n]*(step))) # write each line               
            myuart.write('stop') # let spyder know when data capturing is done
            pinA5.low()   # turn off LED when everything has been written 
            val = 0 # clear val
            break
        else:
            pass