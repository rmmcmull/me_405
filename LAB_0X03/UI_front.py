'''
@file UI_front.py

@brief This script acts as the front end for lab 2

@details This code sends an ASCII character to the nucleo board on which a backend
          program is running. This back end program will only execute its function
          if ctrl+g was entered and sent to the board. Once ctrl+g is entered, 
          the backend code will use an ADC converter to record the voltage of a button push
          over time. Both the voltage and associated time will be sent back through the serial
          line and be delivered. This data will then be plotted.The on-board LED 
          will turn on after a delay of 4 seconds. As soon as the 
          LED goes high, the user must push the button as fast as possible, as was 
          done in the reaction time lab 2. The delay after the LED goes high may need
          to be adjusted based on reaction time. It has been tuned for a reaction 
          time of .25s, which is average. Once data is collected, the data is written
          to a .csv file.
          
@author  Ryan McMullen

@copyright simpout controls

@date February 3, 2021
'''

import serial, csv
#import math
from matplotlib import pyplot as plt #module which encapsulates access for serial port 

## marker which can be used to control flow of states in FSM
state = 0 # always start by going to the initilization state

## voltage data from button press
volt = []
## time data associated with collected voltage from button press
time = []
## variable used as an intermediary for data collection
value = 0
## variable used as an intermediary for data collection
values = 0
## variable used to index data
n = 0
       

def sendChar(inv):
    '''
    @brief This function communicates with the nucleo. 
            
    @details This function will be called if and only if ctrl+g has been entered.
            This function simply communicates, through the COM3 serial port, with the
            nucleo, letting it know to collect button bounce data.
    '''
    ser.write(str(inv).encode('ascii')) # writes data to the serial port.
                                       
def plotData(time,signal):
    '''
    @brief plots a graph with a specified format
    '''
    plt.figure()
    plt.plot(time, signal, 'bo')
    plt.title('ADC Data')
    plt.xlabel('Time [ms]')
    plt.ylabel('ADC Reading [V]')
    plt.show()

# first set up COM3 serial port. the baudrate represents the number of symbols
# which are trasmitted per second. If no communication occurs for more than 
# 10 seconds, the program will timeout.g
## set up the COM3 serial port
ser = serial.Serial(port='COM3',baudrate=115200,timeout=2)
ser.flushInput() # empty the register

while True:
    try:
        if state == 0: # Initilization state
            state = 1 # go to state 1
            
        elif state == 1: # state in which program waits for user to enter a character
			## allows user to input, with a prompt "give me a character"
            inv = input('Input G to collect data: ') # allows user to input, with prompt 
                                                 # "give me a character:"                         
            if inv == 'G': # if user enters ctrl+g
                sendChar(inv) # tell the board that.. we want button bounce data!
                print('''G has been inputted 
                  
                         wait for LED to come on (3[s] delay) and press button ASAP
                         collect good data.
                      ''')    
                state = 2 # go to state 2, where front end will wait for backend to 
                          # obtain nessesary information.
            else: # if a character other than ctrl+g was pressed go here
                inv = 0 #  clear whatever input was entered by the user
                print('enter G or leave') # communicate to the user that ctrl+g 
                                         # needs to be pressed in order for data
                                         # collection.
        elif state == 2: # wait, recieve, and process data from backend state
            value = ser.readline()
            
			## intermediary for voltage data coming from the button bounce
            change = value.decode()
            
            values = change.strip().split(',')
            print(values)
            if values != '[stop]':
                try:
					## voltage from board
                    volts=float(values[0])
                except ValueError:
                    pass
                else:
                     volt.append(volts) # fill out volt list
                     time.append(2e-3*n) # fill out time list
                     n+=1 #
                
            else:
                    
                break
            
    except KeyboardInterrupt:
            print('ctrl+c has been pressed - exited..')
			## csv file containing data
            f = open("button_step_response.csv","w")
            for n in range (len(volt)): # loop n = len(buf) 
				## voltage and associated time data
                tup1 = (volt[n],time[n])
				## used to write data to csv file-
                writer = csv.writer(f)
                writer.writerow(tup1)
            f.close()
            plotData(time,volt)
            break      
        
ser.close()