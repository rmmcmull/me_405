'''
@file LAB_0X02.py

@brief Reaction Time Test!

@details    This program waits a random time between 2 and 3 seconds, then
            turns on the little green LED connected to Pin PA5 on the Nucleo
            L476RG Microcontroller. Once the LED is turned on, a timer starts
            which counts in microseconds. The LED will only remain on for 1 
            second. An external interrupt is set to trigger on the falling edge
            on PC13, which occurs when the blue "user" button on the Nucleo is
            pressed. The interupt callback function then reads the timer to 
            see how many microseconds have elapsed since the LED was turned
            on. This process is repeated until the user presses Ctrl+C to 
            stop the program. When the program is stopped, the average reaction
            time over the number of tries is calculated and displayed. 
            If no reaction time was successfully measured (such as the button
            wasn't pressed), the program wil return an error message. 
            State 0 is the initilization state, in which all 
               variables related to calculating the average reaction time are 
               cleared. Once initialized, program will go into state 1; in state 
               1, a random number between 2000 and 3000 milliseconds is generated 
               and used to delay. Once out of the delay, the program moves into state 2. 
               In state 2, the LED is turned on, and a counter is started, which
               counts in microseconds. The start time is recorded and the program 
               gets pushed into a while loop within state 2. While in this nested 
               loop, there are two possible paths: the first is that the user 
               presses the button before 1 second has elapsed while the LED is high. 
               The other is that the user does not push the button within the 1 
               second window. If a button was pushed, a message will print, the
               elapsed time will be calculated, the LED will remain on for the
               rest of the 1s, and the FSM will transition to state 3. In state
               3, the elapsed time will be added to a running total. Additionally,
               the total number of attempts will be recorded. This FSM will 
               repeat until the user hits ctrl-c. When ctrl-c is pressed, the 
               total time gets divided by the number of tries and the average 
               reaction time is calculated.

@author Ryan M. McMullen

@copyright simpout controls

@date January 27, 2021
'''
# import required libraries
import pyb # Import library which is related to the Nucleo board
import random # Import library which supports random time for reaction
import utime

## marker used to transition to states in FSM
state = 0
## time at which the timer starts for the LED
start_count = 0 

## define timer 2 for general purpose counting at a frequency of 10Hz
tim = pyb.Timer(2, prescaler = 79, period = 0x7FFFFFFF)
## period of timer
period = 2147483647

# define pins and their function
## pin C13 on the nuclceo (blue button)
pinC13 = pyb.Pin (pyb.Pin.cpu.C13) 
## define pin A5 on the nucleo (LED)
pinA5 = pyb.Pin (pyb.Pin.cpu.A5, pyb.Pin.OUT_PP) 


def onButtonPressFCN(IRQ_src): # function which is called back in the event of a button push
    '''
    @brief      Function which is called back in the event of a button push
    @details    When the blue button is pushed on the nucleo microcontroller, 
                this function is called. This function then records the time,
                in microseconds, at which the button was pushed. Finally, it 
                pushes the FSM into state 3 to record important reaction time 
                data.
	@param stop_time time at which the user pressed the blue button
	@param button_pressed boolean that indicates button has been pressed
    '''   
    # record time  button pushed (make global)                       
    global stop_time
    global button_pressed
	## time at which the user pressed the blue button
    stop_time = tim.counter()
	## boolean that indicates button has been pressed
    button_pressed = 1 # set flag if button has been pressed
    
def randomDelayFCT():
    '''
    @brief      function which generates a random number  
    @details     This function generates a random number between 2000 and 3000 
                milliseconds. No value is returned, instead the integer variable
                is made global.
	@param ranNum random number between 2 and 3
    '''
    global ranNum
    ranNum = random.randrange(2000,3000)
    
def avgTimeFCT(tot_time,num_blink):
    '''
    @brief     Function calculates the average time
    @details    This function simply calculates the average time by diving the 
               total time (sum of all elapsed times) by the total number of 
               times the game was played. The inputs are total time and number 
               of plays. The function returns the average time as a floating 
               point number.
   @param avg_time average reaction time of user
    '''
    
    avg_time = tot_time / num_blink
    return avg_time
## interrupt when button gets pressed
ButtonInt = pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_FALLING,pull=pyb.Pin.PULL_NONE, callback=onButtonPressFCN)
# class pyb.ExtInt(pin, mode, pull, callback) this constructor calls function onButtonPressFCN
# when pinC13 enables an interrupt. Interrupt is triggered on falling edge.

while True:
	
    try: # try block allows code to be tested and works with except block to allow
         # user to handle certain errors

        if  state == 0: # Initilization State
		    ## contains the number of times user has tested
            num_blink = 0 
			## contains total reaction time (sum of all rct_time)
            tot_time = 0 
			## average reaction time
            avg_time = 0 
			## time at which button was pressed
            stop_time = 0 
			## reaction time
            elapsed_time = 0 
			## flag which indicates if button has been pressed
            button_pressed = 0 
			## used to calculate extra time needed to delay LED 1s
            time_left = 0 
			## always move into state 1
            state = 1 
            
        elif state == 1: # wait for first light
            randomDelayFCT() # generates random time in milliseconds
            pyb.disable_irq() #disable interupts
            utime.sleep_ms(ranNum) # delay a random number of time between 2s and 3s
            pyb.enable_irq() # reenable interupts
            state = 2 # go to first light

        elif state == 2: # first light! and start the timer
			## if anyone pressed button during delay, it will be cleared
            button_pressed = 0 
			## if anyone pressed button during delay, time will be cleared
            stop_time = 0 
            pinA5.high() # turn on LED!
            tim.counter(0) # set counter value to zero
			## start the timer
            start_time = tim.counter() 
            
            while state == 2:
                if tim.counter() - start_time < 1000000 and button_pressed == 1:
                    pyb.disable_irq() # diable interrupts
                    print('button pressed') # give user visual indication if button pushed
					## elapsed time between LED and button press
                    elapsed_time = stop_time - start_time 
					## time left for the LED to be on for 1 second
                    time_left = (1000000 - elapsed_time) 
                    pyb.udelay(time_left) # enable interrupts
                    pinA5.low() # turn off LED after 1 second
                    state = 3 # go to state 3
                    pyb.enable_irq() # enable irq
                    
                elif tim.counter() - start_time >= 1000000:
                    pinA5.low() # turn off LED after 1 second
                    state = 4 # to to state 4

            
        elif state == 3: # button was pressed
            state = 1 # transition back to state 1 after data is recorded
			## total time of all trails
            tot_time += elapsed_time 
            num_blink += 1 # total number of times game has been played
            button_pressed = 0 # clear button pressed flag
            
        
        elif state == 4: # button was not pressed
            print('button was not pressed -- no time recorded!')
            button_pressed = 0
            state = 1
            
    except KeyboardInterrupt:
        if tot_time > 0:
            print('Ctrl+c pressed -- results are as follows:')
            avg_time =  avgTimeFCT(tot_time, num_blink)
            avg_time /= 1000000
            print('number of tries: ' + str(num_blink) + ' || average reaction time: ' + str(avg_time) + '[s]')
        elif tot_time == 0:
            print('no buttons pressed, try again')
        break